extends Node

onready var exepath = ProjectSettings.globalize_path("res://")
onready var nirpath = exepath + "nircmd.exe"
var username

func _ready():
	if OS.has_environment("USERNAME"):
    	username = OS.get_environment("USERNAME")
	pass
	
func set_app_volume(name,vol):
	var output = []
	OS.execute(Globals.nirpath,["setappvolume", name, String(vol)],false,output)
	return output

func is_app_running(name):
	var output = []
	OS.execute("Tasklist",[""],false,output)
	print(output)
	

